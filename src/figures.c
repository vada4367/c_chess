#include "c_chess.h"

#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// After 1 month I finally decided, that I
// need make standart of figures functions
//
// So it is standart now:
//
// bool figure(Chess *chess, bool mutable, int x, int y, int dx, int dy, bool
// logs);
//
// Reasons:
// 1. Pawn is really brainf*ck me, because it has en passant.
// So if you need to make it, you need to pointer of chess.
//
// 2. Also, I need to make function, which give all squares in attacking.
// So, also f*cking pawn. If i just check, can I make attack by pawn,
// It will use en passant. So I need mutable for chess.
//
// 3. And, can I just check move? NOOO!!! So logs also will be in functions
// for silent use.
//
// 4. X, Y, DX, DY I need for typical reasons.

bool pawn(Chess *chess, bool mutable, int x, int y, int dx, int dy, bool logs) {
  int up = chess->side_move ? -1 : 1;

  int pawn_moves[][2] = PAWN_MOVES;

  if (chess->side_move)
    dy = -dy;

  int i = 0;
  for (; i < 4; ++i)
    if (i < 4 && dx == pawn_moves[i][0] && dy == pawn_moves[i][1])
      break;
  if (i == 4) {
    if (logs)
      fmove_err("Impossible move Pown", 2, dx, dy);
    return false;
  }

  if (dy == 2 &&
      ((!chess->side_move && y != 1) || (chess->side_move && y != 6))) {
    if (logs)
      fmove_err("Impossible move Pown: not first move", 2, dx, dy);
    return false;
  }

  if (dy == 2 &&
      (chess->board[y + up][x] != ' ' || chess->board[y + 2 * up][x] != ' ')) {
    if (logs)
      fmove_err("Impossible move Pown: other figure on the way", 2, dx, dy);
    return false;
  }

  if (dy == 1 && dx == 0 && chess->board[y + up][x] != ' ') {
    if (logs)
      fmove_err("Impossible move Pown: other figure on the way", 2, dx, dy);
    return false;
  }

  int y_en_passant = chess->en_passant >> 4;
  int x_en_passant = (chess->en_passant - (y_en_passant << 4));

  if (dy == 1 && dx != 0 && y == y_en_passant && x + dx == x_en_passant &&
      chess->en_passant != 0) {
    if (mutable)
      chess->board[y][x + dx] = ' ';
    return true;
  }

  if (dy == 1 && dx != 0 && chess->board[y + up][x + dx] == ' ') {
    if (logs)
      fmove_err("Impossible move Pown: no figure on attack", 2, dx, dy);
    return false;
  }

  if (dy == 2 && mutable) {
    uint8_t new_en_passant = 0;
    new_en_passant |= (y + (chess->side_move ? -dy : dy));
    new_en_passant = new_en_passant << 4;
    new_en_passant |= (x + dx);
    chess->en_passant = new_en_passant;
  } else if (mutable)
    chess->en_passant = 0;

  return true;
}

bool pawn_attack(Chess *chess, bool mutable, int x, int y, int dx, int dy,
                 bool logs) {
  int pawn_moves[][2] = PAWN_ATTACKS;

  if (chess->side_move)
    dy = -dy;

  int i = 0;
  for (; i < 2; ++i)
    if (i < 2 && dx == pawn_moves[i][0] && dy == pawn_moves[i][1])
      break;
  if (i == 2) {
    if (logs)
      fmove_err("Impossible move Pown", 2, dx, dy);
    return false;
  }

  int y_en_passant = chess->en_passant >> 4;
  int x_en_passant = (chess->en_passant - (y_en_passant << 4));

  if (dy == 1 && dx != 0 && y == y_en_passant && x + dx == x_en_passant &&
      chess->en_passant != 0) {
    if (mutable)
      chess->board[y][x + dx] = ' ';
    return true;
  }

  if (dy == 2 && mutable) {
    uint8_t new_en_passant = 0;
    new_en_passant |= (y + dy);
    new_en_passant = new_en_passant << 4;
    new_en_passant |= (x + dx);
    chess->en_passant = new_en_passant;
  } else if (mutable)
    chess->en_passant = 0;

  return true;
}

bool knight(Chess *chess, bool mutable, int x, int y, int dx, int dy,
            bool logs) {
  if (!((abs(dx) == 2 && abs(dy) == 1) || (abs(dx) == 1 && abs(dy) == 2))) {
    if (logs)
      fmove_err("Impossible move Knight", 2, dx, dy);
    return false;
  }

  return true;
}

bool rook(Chess *chess, bool mutable, int x, int y, int dx, int dy, bool logs) {
  if (!((dx == 0 && abs(dy) < 8) || (dy == 0 && abs(dx) < 8))) {
    if (logs)
      fmove_err("Impossible move Rook", 2, dx, dy);
    return false;
  }

  if (dx == 0) {
    for (int i = dy / abs(dy); abs(i) < abs(dy); i += dy / abs(dy)) {
      if (chess->board[y + i][x] != ' ' && i != dy) {
        if (logs)
          fmove_err("Rook attacking", 2, dx, dy);
        return false;
      }
    }
  }

  if (dy == 0) {
    for (int i = dx / abs(dx); abs(i) < abs(dx); i += dx / abs(dx)) {
      if (chess->board[y][x + i] != ' ' && i != dx) {
        if (logs)
          fmove_err("Rook attacking", 2, dx, dy);
        return false;
      }
    }
  }

  if (toupper(chess->board[y][x]) == 'R') {
    if (!mutable)
      return true;

    if (x == chess->w - 1)
      chess->castling &=
          UINT8_MAX ^
          (chess->side_move ? CASTLING_WHITE_KING : CASTLING_BLACK_KING);
    if (x == 0)
      chess->castling &=
          UINT8_MAX ^
          (chess->side_move ? CASTLING_WHITE_QUEEN : CASTLING_BLACK_QUEEN);
  }

  return true;
}

bool bishop(Chess *chess, bool mutable, int x, int y, int dx, int dy,
            bool logs) {
  if (abs(dx) != abs(dy)) {
    if (logs)
      fmove_err("Impossible move Bishop", 2, dx, dy);
    return false;
  }

  for (int i = dx / abs(dx), j = dy / abs(dy); abs(i) < abs(dx);
       i += dx / abs(dx), j += dy / abs(dy)) {
    if (x + i < 0 || x + i >= chess->w || y + j < 0 || y + j >= chess->h)
      return false;
    if (chess->board[y + j][x + i] != ' ' && i != dx && j != dy) {
      if (logs)
        fmove_err("Bishop attacking", 2, dx, dy);
      return false;
    }
  }

  return true;
}

bool queen(Chess *chess, bool mutable, int x, int y, int dx, int dy,
           bool logs) {
  if (!(rook(chess, mutable, x, y, dx, dy, logs) ||
        bishop(chess, mutable, x, y, dx, dy, logs)))
    return false;

  return true;
}

static bool castling_function(Chess chess, int dx) {
  int y = chess.side_move ? 7 : 0;
  int x = 4;

  uint64_t attack_squares = get_attack_squares(chess);

  if (dx == 2) {
    if (toupper(chess.board[y][x + 3]) != 'R')
      return false;
    if (chess.board[y][x + 1] != ' ' || chess.board[y][x + 2] != ' ')
      return false;
    for (int i = 0; i < 3; ++i)
      if (attack_squares & (uint64_t)1 << (x + i + y * chess.w))
        return false;
  }
  if (dx == -2) {
    if (toupper(chess.board[y][x - 4]) != 'R')
      return false;
    if (chess.board[y][x - 1] != ' ' || chess.board[y][x - 2] != ' ' ||
        chess.board[y][x - 3] != ' ')
      return false;
    for (int i = 0; i > -3; --i)
      if (attack_squares & (uint64_t)1 << (x + i + y * chess.w))
        return false;
  }

  return true;
}

bool king(Chess *chess, bool mutable, int x, int y, int dx, int dy, bool logs) {
  int king_moves[][2] = KING_MOVES;
  int i = 0;
  for (; i < 10; ++i)
    if (i < 10 && dx == king_moves[i][0] && dy == king_moves[i][1])
      break;
  if (i == 10) {
    if (logs)
      fmove_err("Impossible move King", 2, dx, dy);
    return false;
  }

  if (abs(dx) == 2 && dy != 0) {
    if (logs)
      fmove_err("Impossible Castling", 1, dy);
    return false;
  }

  int castling[2]; // KQ or kq
  if (chess->side_move) {
    castling[0] = chess->castling & CASTLING_WHITE_KING;
    castling[1] = chess->castling & CASTLING_WHITE_QUEEN;
  } else {
    castling[0] = chess->castling & CASTLING_BLACK_KING;
    castling[1] = chess->castling & CASTLING_BLACK_QUEEN;
  }

  if ((dx == 2 && !castling[0]) || (dx == -2 && !castling[1])) {
    if (logs)
      fmove_err("Impossible Castling", 1, chess->castling);
    return false;
  }

  if (abs(dx) == 2 && !castling_function(*chess, dx)) {
    if (logs)
      fmove_err("Impossible Castling, castling_function returned false", 0);
    return false;
  } else {
    int y = chess->side_move ? 7 : 0;
    // unsafe_move(chess, 4, y, 4 + dx, y);
    if (dx == 2 && mutable) {
      chess->board[y][5] = chess->board[y][7];
      chess->board[y][7] = ' ';
    }
    if (dx == -2 && mutable) {
      chess->board[y][3] = chess->board[y][0];
      chess->board[y][0] = ' ';
    }
  }

  if (!((abs(dx) == 0 && abs(dy) == 1) || (abs(dx) == 1 || abs(dy) == 0) ||
        (abs(dx) == 1 || abs(dy) == 1))) {
    if (logs)
      fmove_err("Impossible move King", 2, dx, dy);
    return false;
  }

  if (mutable) {
    chess->castling &=
        (uint8_t)0b11111111 ^
        (chess->side_move ? CASTLING_WHITE_KING : CASTLING_BLACK_KING);
    chess->castling &=
        (uint8_t)0b11111111 ^
        (chess->side_move ? CASTLING_WHITE_QUEEN : CASTLING_BLACK_QUEEN);
  }

  return true;
}

/*
void
unsafe_move (chess->*chess-> char *move)
{
  chess->>chess->][]
      = chess->>chess->7 - (move[1] - '1')][move[0] - 'a'];
  chess->>chess->7 - (move[1] - '1')][move[0] - 'a'] = ' ';
  chess->>side_move = !chess->>side_move;
}
*/
