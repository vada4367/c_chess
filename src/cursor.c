#include "c_chess.h"

#include <ctype.h>
#include <stdio.h>

#ifdef RAYLIB
#include "raylib.h"

static bool cursor_pressed(void) {
#ifdef PLATFORM_ANDROID
  return GetTouchPointCount() > 0;
#else
  return IsMouseButtonDown(MOUSE_BUTTON_LEFT);
#endif
}

void control(Chess *chess, Vector2 *last_touch) {
  int graph_board_x;
  int graph_board_y;
  int graph_board_a;

  get_graphic_board(*chess, &graph_board_x, &graph_board_y, &graph_board_a);

  int scr_w = GetScreenWidth();

  Rectangle queen_rec = {scr_w * 0.5 - graph_board_a * 1.75 * 2,
                         graph_board_y +
                             ((float)chess->h / 2 - 1.75 / 2) * graph_board_a,
                         graph_board_a * 1.75, graph_board_a * 1.75};
  Rectangle rook_rec = {scr_w * 0.5 - graph_board_a * 1.75,
                        graph_board_y +
                            ((float)chess->h / 2 - 1.75 / 2) * graph_board_a,
                        graph_board_a * 1.75, graph_board_a * 1.75};
  Rectangle bishop_rec = {scr_w * 0.5,
                          graph_board_y +
                              ((float)chess->h / 2 - 1.75 / 2) * graph_board_a,
                          graph_board_a * 1.75, graph_board_a * 1.75};
  Rectangle knight_rec = {scr_w * 0.5 + graph_board_a * 1.75,
                          graph_board_y +
                              ((float)chess->h / 2 - 1.75 / 2) * graph_board_a,
                          graph_board_a * 1.75, graph_board_a * 1.75};

  //

#ifdef PLATFORM_ANDROID

  Vector2 touch_pos = {0};
  int cursor_board_x, cursor_board_y;
  int cursor_x, cursor_y;
  if (GetTouchPointCount() > 0) {
    touch_pos = GetTouchPosition(0);

    cursor_x = touch_pos.x;
    cursor_y = touch_pos.y;
    cursor_board_x = ((int)touch_pos.x - graph_board_x) / graph_board_a;
    cursor_board_y = ((int)touch_pos.y - graph_board_y) / graph_board_a;
    last_touch->x = touch_pos.x;
    last_touch->y = touch_pos.y;
  } else {
    cursor_x = last_touch->x;
    cursor_y = last_touch->y;
    cursor_board_x = ((int)last_touch->x - graph_board_x) / graph_board_a;
    cursor_board_y = ((int)last_touch->y - graph_board_y) / graph_board_a;
  }

#else

  Vector2 mouse_pos = GetMousePosition();

  int cursor_x = mouse_pos.x;
  int cursor_y = mouse_pos.y;
  int cursor_board_x = ((int)mouse_pos.x - graph_board_x) / graph_board_a;
  int cursor_board_y = ((int)mouse_pos.y - graph_board_y) / graph_board_a;

#endif

  char promotion_figure = ' ';

  if (chess->promotion[0] != -1 && cursor_pressed()) {
    if (CheckCollisionPointRec((Vector2){cursor_x, cursor_y}, queen_rec))
      promotion_figure = chess->promotion[1] == 0 ? 'Q' : 'q';
    if (CheckCollisionPointRec((Vector2){cursor_x, cursor_y}, rook_rec))
      promotion_figure = chess->promotion[1] == 0 ? 'R' : 'r';
    if (CheckCollisionPointRec((Vector2){cursor_x, cursor_y}, bishop_rec))
      promotion_figure = chess->promotion[1] == 0 ? 'B' : 'b';
    if (CheckCollisionPointRec((Vector2){cursor_x, cursor_y}, knight_rec))
      promotion_figure = chess->promotion[1] == 0 ? 'N' : 'n';
  }

  if (promotion_figure != ' ') {
    chess->board[chess->promotion[1]][chess->promotion[0]] = promotion_figure;
    chess->promotion[0] = -1;
    chess->promotion[1] = -1;
  }

  else if (chess->figure_in_keep[0] == -1 && cursor_pressed() &&
           cursor_board_x >= 0 && cursor_board_x < chess->w &&
           cursor_board_y >= 0 && cursor_board_y < chess->h &&
           chess->board[cursor_board_y][cursor_board_x] != ' ' &&
           ((isupper(chess->board[cursor_board_y][cursor_board_x]) &&
             chess->side_move) ||
            (islower(chess->board[cursor_board_y][cursor_board_x]) &&
             !chess->side_move)))

  {
    chess->figure_in_keep[0] = cursor_board_y;
    chess->figure_in_keep[1] = cursor_board_x;
  }

  if (chess->figure_in_keep[0] != -1 && !cursor_pressed()) {
    if (cursor_board_x >= 0 && cursor_board_x < chess->w &&
        cursor_board_y >= 0 && cursor_board_y < chess->h) {
      safe_move(chess, chess->figure_in_keep[1], chess->figure_in_keep[0],
                cursor_board_x, cursor_board_y, false);
    }

    chess->figure_in_keep[0] = -1;
    chess->figure_in_keep[1] = -1;
  }
}

#endif
