#include "c_chess.h"
#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef RAYLIB
#include "raylib.h"

void get_graphic_board(Chess board, int *x, int *y, int *a) {
  int scr_w = GetScreenWidth();
  int scr_h = GetScreenHeight();

  if (x != NULL)
    *x = scr_w > scr_h ? (scr_w - scr_h) / 5 : 0;
  if (y != NULL)
    *y = scr_w > scr_h ? 0 : (scr_h - scr_w) / 5;
  if (a != NULL)
    *a = (scr_w > scr_h ? scr_h : scr_w) / board.w;
}

void graphic_pawn_promotion(Chess board, PieceSet piece_set) {
  int scr_w = GetScreenWidth();

  int graph_board_x;
  int graph_board_y;
  int graph_board_a;

  get_graphic_board(board, &graph_board_x, &graph_board_y, &graph_board_a);

  DrawRectangle(0, graph_board_y + (board.h / 2 - 1) * graph_board_a, scr_w,
                2 * graph_board_a, (Color){0xff, 0xff, 0xff, 0xaa});

  Texture2D queen =
      !board.side_move ? get_piece(piece_set, 'Q') : get_piece(piece_set, 'q');
  Texture2D rook =
      !board.side_move ? get_piece(piece_set, 'R') : get_piece(piece_set, 'r');
  Texture2D bishop =
      !board.side_move ? get_piece(piece_set, 'B') : get_piece(piece_set, 'b');
  Texture2D knight =
      !board.side_move ? get_piece(piece_set, 'N') : get_piece(piece_set, 'n');

  DrawTextureEx(queen,
                (Vector2){scr_w * 0.5 - graph_board_a * 1.75 * 2,
                          graph_board_y +
                              ((float)board.h / 2 - 1.75 / 2) * graph_board_a},
                0.0, 1.75, WHITE);
  DrawTextureEx(rook,
                (Vector2){scr_w * 0.5 - graph_board_a * 1.75,
                          graph_board_y +
                              ((float)board.h / 2 - 1.75 / 2) * graph_board_a},
                0.0, 1.75, WHITE);
  DrawTextureEx(
      bishop,
      (Vector2){scr_w * 0.5, graph_board_y + ((float)board.h / 2 - 1.75 / 2) *
                                                 graph_board_a},
      0.0, 1.75, WHITE);
  DrawTextureEx(knight,
                (Vector2){scr_w * 0.5 + graph_board_a * 1.75,
                          graph_board_y +
                              ((float)board.h / 2 - 1.75 / 2) * graph_board_a},
                0.0, 1.75, WHITE);
}

void graphic_print_board(Chess board, PieceSet piece_set,
                         Texture2D board_theme) {
  int scr_w = GetScreenWidth();
  int scr_h = GetScreenHeight();

  int graph_board_x;
  int graph_board_y;
  int graph_board_a;
  get_graphic_board(board, &graph_board_x, &graph_board_y, &graph_board_a);

  DrawTexture(board_theme, graph_board_x, graph_board_y, WHITE);

  uint64_t attack_squares = get_attack_squares(board);
  for (int i = 0; i < board.w; ++i)
    for (int j = 0; j < board.h; ++j)
      if (attack_squares & (uint64_t)1 << (i + j * board.w))
        DrawRectangle(graph_board_x + i * graph_board_a,
                      graph_board_y + j * graph_board_a, graph_board_a,
                      graph_board_a, (Color){0x28, 0, 0, 255 / 2});

  for (int i = 0; i < board.h; ++i) {
    for (int j = 0; j < board.w; ++j) {
      if (board.board[j][i] != ' ') {
        if (board.figure_in_keep[1] == i && board.figure_in_keep[0] == j)
          continue;
        Texture2D figure_texture = get_piece(piece_set, board.board[j][i]);

        DrawTexture(figure_texture, graph_board_x + i * graph_board_a,
                    graph_board_y + j * graph_board_a, WHITE);
      }
    }
  }

  if (board.figure_in_keep[0] != -1) {
    Texture2D figure_texture = get_piece(
        piece_set,
        board.board[board.figure_in_keep[0]][board.figure_in_keep[1]]);

    Vector2 cursor_pos = {-1, -1};
#ifdef PLATFORM_ANDROID

    if (GetTouchPointCount() > 0)
      cursor_pos = GetTouchPosition(0);

#else

    cursor_pos = GetMousePosition();

#endif
    if (cursor_pos.x != -1)
      DrawTextureEx(figure_texture,
                    (Vector2){cursor_pos.x - graph_board_a * 0.75,
                              cursor_pos.y - graph_board_a * 0.75},
                    0.0, 1.5, WHITE);
  }

  for (int i = 0; i < board.w; ++i)
    if (toupper(board.board[0][i]) == 'P' || toupper(board.board[7][i]) == 'P')
      graphic_pawn_promotion(board, piece_set);
}

#endif

void free_chess(Chess chess) {
  for (int i = 0; i < chess.h; ++i)
    free(chess.board[i]);

  free(chess.board);
}

Chess copy_chess(Chess chess) {
  Chess copy;

  copy.w = chess.w;
  copy.h = chess.h;

  copy.board = malloc(sizeof(char *) * copy.h);
  for (int i = 0; i < copy.h; ++i) {
    copy.board[i] = malloc(sizeof(char) * copy.w);
    for (int j = 0; j < copy.w; ++j)
      copy.board[i][j] = chess.board[i][j];
  }

  copy.castling = chess.castling;
  copy.side_move = chess.side_move;
  copy.en_passant = chess.en_passant;
  copy.fullmove_clock = chess.fullmove_clock;
  copy.halfmove_clock = chess.halfmove_clock;
  copy.figure_in_keep[0] = chess.figure_in_keep[0];
  copy.figure_in_keep[1] = chess.figure_in_keep[1];
  copy.promotion[0] = chess.promotion[0];
  copy.promotion[1] = chess.promotion[1];

  return copy;
}

int *str_to_move(char *move) {
  int *result_move = malloc(sizeof(int) * 4);
  result_move[0] = move[0] - 'a';
  result_move[1] = 7 - (move[1] - '1');
  result_move[2] = (move[2] - 'a') - result_move[0];
  result_move[3] = (7 - (move[3] - '1')) - result_move[1];

  return result_move;
}

// This function only for checking on impossible moves by all figures
bool can_move(Chess chess, int move[4], bool logs) {
  if (move[0] < 0 || move[0] >= chess.w || move[2] < 0 ||
      move[2] >= chess.w || move[1] < 0 || move[1] >= chess.h ||
      move[3] < 0 || move[3] >= chess.h) {
    if (logs)
      fmove_err("Unexpected input. Move should looks like g2g4\n!!! Where "
                "syms >=a and <=h, and numbers >0 and <9",
                0);
    return false;
  }

  if (chess.board[move[1]][move[0]] == ' ') {
    if (logs)
      fmove_err("Just stop moving empty figures, pls", 0);
    return false;
  }

  if ((isupper(chess.board[move[1]][move[0]]) && !chess.side_move) ||
      (!isupper(chess.board[move[1]][move[0]]) && chess.side_move)) {
    if (logs)
      fmove_err("You cannot move pieces of your opponent", 0);
    return false;
  }

  if (chess.board[move[3]][move[2]] != ' ' &&
      ((isupper(chess.board[move[3]][move[2]]) && chess.side_move) ||
       (!isupper(chess.board[move[3]][move[2]]) && !chess.side_move))) {
    if (logs)
      fmove_err("Stop cannibalism", 0);
    return false;
  }

  return true;
}

bool chess_check(Chess copy) {
  uint64_t attack_squares_after_move = get_attack_squares(copy);

  for (int i = 0; i < copy.w; ++i)
    for (int j = 0; j < copy.h; ++j)
      if (((copy.board[j][i] == 'K' && !copy.side_move) ||
           (copy.board[j][i] == 'k' && copy.side_move)) &&
          attack_squares_after_move & (uint64_t)1 << (i + j * copy.w))
        return true;

  return false;
}

void unsafe_move(Chess *chess, int x1, int y1, int x2, int y2) {
  chess->board[y2][x2] = chess->board[y1][x1];
  chess->board[y1][x1] = ' ';

  chess->side_move = !chess->side_move;
}

// Function for using by playing
int safe_move(Chess *chess, int x1, int y1, int x2, int y2, bool logs) {
  int move[4] = {x1, y1, x2, y2};

  if (!can_move(*chess, move, logs))
    return 1;

  // Pointer on function
  bool (*figure_check_move)(Chess *, bool, int, int, int, int, bool);
  switch (toupper(chess->board[move[1]][move[0]])) {
  case 'P':
    figure_check_move = pawn;
    break;
  case 'N':
    figure_check_move = knight;
    break;
  case 'R':
    figure_check_move = rook;
    break;
  case 'B':
    figure_check_move = bishop;
    break;
  case 'Q':
    figure_check_move = queen;
    break;
  case 'K':
    figure_check_move = king;
    break;
  }

  if (!figure_check_move(chess, false, move[0], move[1], move[2] - move[0],
                         move[3] - move[1], logs))
    return 1;

  figure_check_move(chess, true, move[0], move[1], move[2] - move[0], move[3] - move[1], logs);

  Chess copy = copy_chess(*chess);
  
  unsafe_move(&copy, move[0], move[1], move[2], move[3]);
 
  copy.side_move = !copy.side_move;
  uint64_t attack_squares_after_move = get_attack_squares(copy);
  copy.side_move = !copy.side_move;

  for (int i = 0; i < copy.w; ++i)
    for (int j = 0; j < copy.h; ++j)
      if (((copy.board[j][i] == 'K' && !copy.side_move) ||
           (copy.board[j][i] == 'k' && copy.side_move)) &&
          attack_squares_after_move & (uint64_t)1 << (i + j * copy.w))
        return 1;

  free_chess(copy);

  unsafe_move(chess, move[0], move[1], move[2], move[3]);

  if (chess->promotion[0] == -1 &&
      toupper(chess->board[move[3]][move[2]]) == 'P' &&
      (move[3] == 0 || move[3] == chess->h - 1)) {
    chess->promotion[0] = move[2];
    chess->promotion[1] = move[3];
  }

  if (toupper(chess->board[move[1]][move[0]]) == 'P' ||
      chess->board[move[3]][move[2]] != ' ')
    chess->halfmove_clock = 0;
  else
    chess->halfmove_clock++;
  chess->fullmove_clock++;

  return 0;
}

void print_board(Chess board) {
  printf("w: %d, h: %d\n", board.w, board.h);
  printf("current move of side: %s\n", board.side_move ? "white" : "black");
  printf("castling: %u\n", board.castling);
  printf("en_passant: %u\n", board.en_passant);
  printf("halfmove_clock: %d\n", board.halfmove_clock);
  printf("fullmove_clock: %d\n", board.fullmove_clock);
  uint64_t attack_squares = get_attack_squares(board);
  printf("attack_squares: %llu\n", attack_squares);

  printf("\n");
  for (int i = 0; i < board.h; ++i) {
    for (int j = 0; j < board.w; ++j) {
      printf("%c ", board.board[i][j]);
      if (j >= board.w - 1) {
        printf("  ");
        for (int k = 0; k < 8; ++k) {
          if (attack_squares & ((uint64_t)1 << (k + i * board.w)))
            printf("# ");
          else
            printf(". ");
        }
      }
    }
    printf(" %d\n", board.h - i);
  }

  printf("\n");
  for (int i = 0; i < board.w; ++i)
    printf("%c ", (char)i + 'a');

  printf("\n");
  for (int i = 0; i < board.w * 4 + 4; ++i)
    printf("-");
  printf("\n");
}

uint64_t get_attack_squares(Chess chess) {
  chess.side_move = !chess.side_move;

  int up = chess.side_move ? -1 : 1;

  // Attacks of all figures
  int p_a[][2] = PAWN_ATTACKS;
  int n_a[][2] = KNIGHT_MOVES;
  int r_a[][2] = ROOK_MOVES;
  int b_a[][2] = BISHOP_MOVES;
  int q_a[][2] = QUEEN_MOVES;
  int k_a[][2] = KING_ATTACKS;

  uint64_t result = 0;
  for (int x = 0; x < chess.w; ++x) {
    for (int y = 0; y < chess.h; ++y) {
      if (chess.board[y][x] != ' ') {
        bool (*figure_move)(Chess *, bool, int, int, int, int, bool);
        int *figure_attack;
        int size_moves;
        switch (toupper(chess.board[y][x])) {
        case 'P': {
          figure_attack = &p_a;
          size_moves = (int)(sizeof(p_a) / sizeof(int) / 2);
          figure_move = pawn_attack;
        } break;
        case 'N': {
          figure_attack = &n_a;
          size_moves = (int)(sizeof(n_a) / sizeof(int) / 2);
          figure_move = knight;
        } break;
        case 'R': {
          figure_attack = &r_a;
          size_moves = (int)(sizeof(r_a) / sizeof(int) / 2);
          figure_move = rook;
        } break;
        case 'B': {
          figure_attack = &b_a;
          size_moves = (int)(sizeof(b_a) / sizeof(int) / 2);
          figure_move = bishop;
        } break;
        case 'Q': {
          figure_attack = &q_a;
          size_moves = (int)(sizeof(q_a) / sizeof(int) / 2);
          figure_move = queen;
        } break;
        case 'K': {
          figure_attack = &k_a;
          size_moves = (int)(sizeof(k_a) / sizeof(int) / 2);
          figure_move = king;
        } break;
        }

        for (int i = 0; i < size_moves; ++i) {
          int move[4] = {x, y, x + figure_attack[i * 2],
                         y + up * figure_attack[i * 2 + 1]};
          if (can_move(chess, move, false) &&
              figure_move(&chess, false, x, y, figure_attack[i * 2],
                          up * figure_attack[i * 2 + 1], false))
            result |= ((uint64_t)1
                       << (x + figure_attack[i * 2] +
                           (y + up * figure_attack[i * 2 + 1]) * chess.w));
        }
      }
    }
  }

  return result;
}
