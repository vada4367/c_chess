#include "c_chess.h"
#include <ctype.h>
#include <string.h>

#ifdef RAYLIB
#include "raylib.h"

PieceSet get_piece_set(char *path, int piece_a) {
  PieceSet ps;

  Image pswP = LoadImage(strcat(my_strndup(path, 100), "wP.png"));
  ImageResize(&pswP, piece_a, piece_a);
  Texture2D wp_text = LoadTextureFromImage(pswP);
  UnloadImage(pswP);
  ps.wP = wp_text;
  Image psbP = LoadImage(strcat(my_strndup(path, 100), "bP.png"));
  ImageResize(&psbP, piece_a, piece_a);
  Texture2D bp_text = LoadTextureFromImage(psbP);
  ps.bP = bp_text;
  UnloadImage(psbP);
  Image pswN = LoadImage(strcat(my_strndup(path, 100), "wN.png"));
  ImageResize(&pswN, piece_a, piece_a);
  Texture2D wn_text = LoadTextureFromImage(pswN);
  ps.wN = wn_text;
  UnloadImage(pswN);
  Image psbN = LoadImage(strcat(my_strndup(path, 100), "bN.png"));
  ImageResize(&psbN, piece_a, piece_a);
  Texture2D bn_text = LoadTextureFromImage(psbN);
  ps.bN = bn_text;
  UnloadImage(psbN);
  Image pswB = LoadImage(strcat(my_strndup(path, 100), "wB.png"));
  ImageResize(&pswB, piece_a, piece_a);
  Texture2D wb_text = LoadTextureFromImage(pswB);
  ps.wB = wb_text;
  UnloadImage(pswB);
  Image psbB = LoadImage(strcat(my_strndup(path, 100), "bB.png"));
  ImageResize(&psbB, piece_a, piece_a);
  Texture2D bb_text = LoadTextureFromImage(psbB);
  ps.bB = bb_text;
  UnloadImage(psbB);
  Image pswR = LoadImage(strcat(my_strndup(path, 100), "wR.png"));
  ImageResize(&pswR, piece_a, piece_a);
  Texture2D wr_text = LoadTextureFromImage(pswR);
  ps.wR = wr_text;
  UnloadImage(pswR);
  Image psbR = LoadImage(strcat(my_strndup(path, 100), "bR.png"));
  ImageResize(&psbR, piece_a, piece_a);
  Texture2D br_text = LoadTextureFromImage(psbR);
  ps.bR = br_text;
  UnloadImage(psbR);
  Image pswQ = LoadImage(strcat(my_strndup(path, 100), "wQ.png"));
  ImageResize(&pswQ, piece_a, piece_a);
  Texture2D wq_text = LoadTextureFromImage(pswQ);
  ps.wQ = wq_text;
  UnloadImage(pswQ);
  Image psbQ = LoadImage(strcat(my_strndup(path, 100), "bQ.png"));
  ImageResize(&psbQ, piece_a, piece_a);
  Texture2D bq_text = LoadTextureFromImage(psbQ);
  ps.bQ = bq_text;
  UnloadImage(psbQ);
  Image pswK = LoadImage(strcat(my_strndup(path, 100), "wK.png"));
  ImageResize(&pswK, piece_a, piece_a);
  Texture2D wk_text = LoadTextureFromImage(pswK);
  ps.wK = wk_text;
  UnloadImage(pswK);
  Image psbK = LoadImage(strcat(my_strndup(path, 100), "bK.png"));
  ImageResize(&psbK, piece_a, piece_a);
  Texture2D bk_text = LoadTextureFromImage(psbK);
  ps.bK = bk_text;
  UnloadImage(psbK);

  return ps;
}

void UnloadPieceSet(PieceSet ps) {
  UnloadTexture(ps.wP);
  UnloadTexture(ps.bP);
  UnloadTexture(ps.wN);
  UnloadTexture(ps.bN);
  UnloadTexture(ps.wB);
  UnloadTexture(ps.bB);
  UnloadTexture(ps.wR);
  UnloadTexture(ps.bR);
  UnloadTexture(ps.wQ);
  UnloadTexture(ps.bQ);
  UnloadTexture(ps.wK);
  UnloadTexture(ps.bK);
}

Texture2D get_piece(PieceSet ps, char piece) {
  switch (toupper(piece)) {
  case 'P':
    return isupper(piece) ? ps.wP : ps.bP;
  case 'N':
    return isupper(piece) ? ps.wN : ps.bN;
  case 'B':
    return isupper(piece) ? ps.wB : ps.bB;
  case 'R':
    return isupper(piece) ? ps.wR : ps.bR;
  case 'Q':
    return isupper(piece) ? ps.wQ : ps.bQ;
  case 'K':
    return isupper(piece) ? ps.wK : ps.bK;
  }
}

#endif
