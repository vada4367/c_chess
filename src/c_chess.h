
#include <inttypes.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct {
  int w;
  int h;

  /* White is 1, Black is 0 */
  bool side_move;

  /*
   * Castling:
   * 4 bits
   * KQkq
   */
  uint8_t castling;

  /*
   * 4 bits for sym:
   * a = 0001
   * 4 bits for num:
   * 1 = 0001
   *
   * cell:
   * a1 = 00010001
   */
  uint8_t en_passant;

  int halfmove_clock;
  int fullmove_clock;
  /* Big sym is While, Smol sym is Black */
  char **board;
  /* For cursor.c */
  int figure_in_keep[2];
  int promotion[2];
} Chess;

#ifdef RAYLIB
#include "raylib.h"

typedef struct {
  Texture2D wP;
  Texture2D wN;
  Texture2D wB;
  Texture2D wR;
  Texture2D wQ;
  Texture2D wK;
  Texture2D bP;
  Texture2D bN;
  Texture2D bB;
  Texture2D bR;
  Texture2D bQ;
  Texture2D bK;
} PieceSet;

void control(Chess *chess, Vector2 *last_touch);

void graphic_print_board(Chess board, PieceSet piece_set,
                         Texture2D board_theme);

PieceSet get_piece_set(char *path, int piece_a);
void UnloadPieceSet(PieceSet ps);
Texture2D get_piece(PieceSet ps, char piece);
#endif

// FROM https://github.com/bminor/musl/blob/master/src/string/strndup.c
size_t my_strnlen(const char *s, size_t n);
char *my_strndup(const char *s, size_t n);


Chess FEN(char *input_string);

void fmove_err(char *err, int num, ...);

void free_chess(Chess chess);
Chess copy_chess(Chess chess);
bool can_move(Chess chess, int move[4], bool logs);
void get_graphic_board(Chess board, int *x, int *y, int *a);
int *str_to_move(char *move);
void unsafe_move(Chess *chess, int x1, int y1, int x2, int y2);
bool chess_check(Chess copy);
int safe_move(Chess *chess, int x1, int y1, int x2, int y2, bool logs);
void print_board(Chess board);
uint64_t get_attack_squares(Chess chess);

bool pawn(Chess *chess, bool mutable, int x, int y, int dx, int dy, bool logs);
bool pawn_attack(Chess *chess, bool mutable, int x, int y, int dx, int dy,
                 bool logs);
bool knight(Chess *chess, bool mutable, int x, int y, int dx, int dy,
            bool logs);
bool rook(Chess *chess, bool mutable, int x, int y, int dx, int dy, bool logs);
bool bishop(Chess *chess, bool mutable, int x, int y, int dx, int dy,
            bool logs);
bool queen(Chess *chess, bool mutable, int x, int y, int dx, int dy, bool logs);
bool king(Chess *chess, bool mutable, int x, int y, int dx, int dy, bool logs);

#define CASTLING_WHITE_KING (1 << 3)
#define CASTLING_WHITE_QUEEN (1 << 2)
#define CASTLING_BLACK_KING (1 << 1)
#define CASTLING_BLACK_QUEEN (1 << 0)

#define PAWN_MOVES                                                             \
  {                                                                            \
    {0, 1}, {0, 2}, {-1, 1}, { 1, 1 }                                          \
  }
#define PAWN_ATTACKS                                                           \
  {                                                                            \
    {-1, 1}, { 1, 1 }                                                          \
  }
#define KNIGHT_MOVES                                                           \
  {                                                                            \
    {-1, 2}, {1, 2}, {-2, 1}, {2, 1}, {-2, -1}, {2, -1}, {-1, -2}, { 1, -2 }   \
  }
#define ROOK_MOVES                                                             \
  {                                                                            \
    {-1, 0}, {-2, 0}, {-3, 0}, {-4, 0}, {-5, 0}, {-6, 0}, {-7, 0}, {0, -1},    \
        {0, -2}, {0, -3}, {0, -4}, {0, -5}, {0, -6}, {0, -7}, {1, 0}, {2, 0},  \
        {3, 0}, {4, 0}, {5, 0}, {6, 0}, {7, 0}, {0, 1}, {0, 2}, {0, 3},        \
        {0, 4}, {0, 5}, {0, 6}, {                                              \
      0, 7                                                                     \
    }                                                                          \
  }
#define BISHOP_MOVES                                                           \
  {                                                                            \
    {-1, -1}, {-2, -2}, {-3, -3}, {-4, -4}, {-5, -5}, {-6, -6}, {-7, -7},      \
        {1, -1}, {2, -2}, {3, -3}, {4, -4}, {5, -5}, {6, -6}, {7, -7},         \
        {-1, 1}, {-2, 2}, {-3, 3}, {-4, 4}, {-5, 5}, {-6, 6}, {-7, 7}, {1, 1}, \
        {2, 2}, {3, 3}, {4, 4}, {5, 5}, {6, 6}, {                              \
      7, 7                                                                     \
    }                                                                          \
  }
#define QUEEN_MOVES                                                            \
  {                                                                            \
    {-1, 0}, {-2, 0}, {-3, 0}, {-4, 0}, {-5, 0}, {-6, 0}, {-7, 0}, {0, -1},    \
        {0, -2}, {0, -3}, {0, -4}, {0, -5}, {0, -6}, {0, -7}, {1, 0}, {2, 0},  \
        {3, 0}, {4, 0}, {5, 0}, {6, 0}, {7, 0}, {0, 1}, {0, 2}, {0, 3},        \
        {0, 4}, {0, 5}, {0, 6}, {0, 7}, {-1, -1}, {-2, -2}, {-3, -3},          \
        {-4, -4}, {-5, -5}, {-6, -6}, {-7, -7}, {1, -1}, {2, -2}, {3, -3},     \
        {4, -4}, {5, -5}, {6, -6}, {7, -7}, {-1, 1}, {-2, 2}, {-3, 3},         \
        {-4, 4}, {-5, 5}, {-6, 6}, {-7, 7}, {1, 1}, {2, 2}, {3, 3}, {4, 4},    \
        {5, 5}, {6, 6}, {                                                      \
      7, 7                                                                     \
    }                                                                          \
  }
#define KING_MOVES                                                             \
  {                                                                            \
    {-1, -1}, {-1, 0}, {-1, 1}, {0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1},      \
        {-2, 0}, {                                                             \
      2, 0                                                                     \
    }                                                                          \
  }
#define KING_ATTACKS                                                           \
  {                                                                            \
    {-1, -1}, {-1, 0}, {-1, 1}, {0, 1}, {1, 1}, {1, 0}, {1, -1}, { 0, -1 }     \
  }

uint64_t count_moves_on_depth(Chess chess, int depth
#ifdef RAYLIB
                         ,
                         PieceSet piece_set, Texture2D board_theme
#endif
);
