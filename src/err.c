#include <stdarg.h>
#include <stdio.h>

// Use it for output errors of moves
void fmove_err(char *err, int num, ...) {
  fprintf(stderr, "\n\n!!! %s ", err);

  va_list arguments;
  va_start(arguments, num);

  if (!(num == 2 || num == 4)) {
    for (int i = 0; i < num; ++i)
      fprintf(stderr, "%d ", va_arg(arguments, int));

    return;
  }

  if (num == 2 || num == 4) {
    fprintf(stderr, "dx: %d ", va_arg(arguments, int));
    fprintf(stderr, "dy: %d ", va_arg(arguments, int));
  }

  if (num == 4) {
    fprintf(stderr, "x: %d ", va_arg(arguments, int));
    fprintf(stderr, "y: %d ", va_arg(arguments, int));
  }

  va_end(arguments);

  fprintf(stderr, "\n\n");
}
