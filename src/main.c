#include "c_chess.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#ifdef RAYLIB
#include "raylib.h"
#endif

int main(void)
{
  Chess board
      = FEN ("rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8");
#ifdef RAYLIB
  InitWindow(0, 0, "Clanguage_CHESS");
  SetWindowSize(GetScreenWidth(), GetScreenHeight());
  SetConfigFlags(FLAG_FULLSCREEN_MODE);
  SetTargetFPS(60);
  //SetTraceLogLevel(LOG_ERROR);
  
  int board_square_size;
  get_graphic_board(board, NULL, NULL, &board_square_size);
#endif


#ifdef PLATFORM_ANDROID
  char theme_path[100] = "";
  ChangeDirectory("assets");
#else
  char theme_path[100] = "./assets/";
#endif

#ifdef RAYLIB
  PieceSet piece_set = get_piece_set(theme_path, board_square_size);

  Image board_theme_i = LoadImage(strcat(my_strndup(theme_path, 100), "board_theme.png"));
  ImageResize(&board_theme_i, board.w * board_square_size, board.h * board_square_size);
  Texture2D board_theme = LoadTextureFromImage(board_theme_i);
  UnloadImage(board_theme_i);
  Vector2 last_touch = {-1, -1};

#endif

#ifdef PLATFORM_ANDROID
  ChangeDirectory("..");
#endif


/*
  //char *input_move = malloc(sizeof(char) * 6);
  for (;;)
  {
    if (WindowShouldClose())
      UnloadPieceSet(piece_set);
    //print_board(board);
    //fgets(input_move, 6, stdin);
    //safe_move(&board, input_move);

    BeginDrawing();
    
    ClearBackground((Color){0x28, 0x28, 0x28, 255});
    graphic_print_board(board, piece_set, board_theme);

    EndDrawing();

    control(&board, &last_touch);
  }
*/


  //board.side_move = !board.side_move;
  int depth = 4;

  printf("depth = %d, nodes = %d\n", depth, count_moves_on_depth(
    board, 
    depth
#ifdef RAYLIB
    ,piece_set, 
    board_theme
#endif
  ));

  //free (input_move);

  return 0;
}
