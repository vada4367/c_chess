#include "c_chess.h"

#include <ctype.h>
#include <stdlib.h>

// I just lost my brain, i forgot this block of code
Chess FEN(char *input_string) {
  int width = 0;
  for (int i = 0; input_string[i] != '/'; ++i) {
    if (isdigit(input_string[i]))
      width += input_string[i] - '0';
    else
      width++;
  }
  int height = 1;
  for (int i = 0; input_string[i]; ++i)
    height += (input_string[i] == '/');

  char **board = malloc(sizeof(char *) * height);
  char *board_string = malloc(sizeof(char) * width);

  int parser_input = 0;
  for (int j = 0; input_string[parser_input] != ' '; ++parser_input) {
    if (isdigit(input_string[parser_input]))
      for (int k = 0; k < (int)input_string[parser_input] - (int)'0'; ++k)
        board_string[j++] = ' ';
    if (isalpha(input_string[parser_input]))
      board_string[j++] = input_string[parser_input];

    if (input_string[parser_input] == '/' ||
        input_string[parser_input + 1] == ' ') {
      *board = board_string;
      board++;
      j = 0;
    }
    if (input_string[parser_input] == '/')
      board_string = malloc(sizeof(char) * width);
  }
  parser_input++;
  board -= height;

  bool side = (input_string[parser_input] == 'w');

  uint8_t castling = 0;
  for (parser_input += 2; input_string[parser_input] != ' '; ++parser_input) {
    switch (input_string[parser_input]) {
    case 'K':
      castling |= CASTLING_WHITE_KING;
      break;
    case 'Q':
      castling |= CASTLING_WHITE_QUEEN;
      break;
    case 'k':
      castling |= CASTLING_BLACK_KING;
      break;
    case 'q':
      castling |= CASTLING_BLACK_QUEEN;
      break;
    }
  }
  parser_input++;

  uint8_t en_passant = 0;
  if (isalpha(input_string[parser_input])) {
    en_passant |= (input_string[parser_input] - 'a' + 1);
    en_passant = en_passant << 4;
    parser_input++;
    en_passant |= (input_string[parser_input] - '0');
  }

  int num_parser = parser_input + 2;
  for (; input_string[num_parser] != ' '; ++num_parser)
    ;
  char *ptr = input_string;
  ptr += parser_input + 2;
  int halfmove_clock = strtol(ptr, &ptr, 10);

  ptr = input_string;
  ptr += num_parser + 1;
  int fullmove_clock = strtol(ptr, &ptr, 10);

  Chess new_board = {width,      height,         side,           castling,
                     en_passant, halfmove_clock, fullmove_clock, board,
                     {-1, -1},   {-1, -1}};
  return new_board;
}
