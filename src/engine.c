#include "c_chess.h"
#include "ctype.h"
#include <ctype.h>
#include <stdint.h>
#include <stdio.h>

#define _GNU_SOURCE
#include <unistd.h>

#ifdef RAYLIB
#include <raylib.h>
#endif

uint64_t count_moves_on_depth(Chess chess, int depth
#ifdef RAYLIB
                         ,
                         PieceSet piece_set, Texture2D board_theme
#endif
) {
  if (depth == 0)
    return 1;

  int result = 0;

  int up = chess.side_move ? -1 : 1;

  // Moves of all figures
  int p_a[][2] = PAWN_MOVES;
  int n_a[][2] = KNIGHT_MOVES;
  int r_a[][2] = ROOK_MOVES;
  int b_a[][2] = BISHOP_MOVES;
  int q_a[][2] = QUEEN_MOVES;
  int k_a[][2] = KING_MOVES;

  for (int x = 0; x < chess.w; ++x) {
    for (int y = 0; y < chess.h; ++y) {
      if (chess.board[y][x] == ' ')
        continue;

      bool (*figure_move)(Chess *, bool, int, int, int, int, bool);
      int *figure_attack;
      int size_moves;
      switch (toupper(chess.board[y][x])) {
      case 'P': {
        figure_attack = &p_a;
        size_moves = (int)(sizeof(p_a) / sizeof(int) / 2);
        figure_move = pawn;
      } break;
      case 'N': {
        figure_attack = &n_a;
        size_moves = (int)(sizeof(n_a) / sizeof(int) / 2);
        figure_move = knight;
      } break;
      case 'R': {
        figure_attack = &r_a;
        size_moves = (int)(sizeof(r_a) / sizeof(int) / 2);
        figure_move = rook;
      } break;
      case 'B': {
        figure_attack = &b_a;
        size_moves = (int)(sizeof(b_a) / sizeof(int) / 2);
        figure_move = bishop;
      } break;
      case 'Q': {
        figure_attack = &q_a;
        size_moves = (int)(sizeof(q_a) / sizeof(int) / 2);
        figure_move = queen;
      } break;
      case 'K': {
        figure_attack = &k_a;
        size_moves = (int)(sizeof(k_a) / sizeof(int) / 2);
        figure_move = king;
      } break;
      }

      for (int i = 0; i < size_moves; ++i) {

#ifdef RAYLIB
        BeginDrawing();
#endif

        int move[4] = {x, y, x + figure_attack[i * 2],
                       y + up * figure_attack[i * 2 + 1]};

        Chess copy_with_move = copy_chess(chess);

        if (safe_move(&copy_with_move, move[0], move[1], move[2], move[3],
                      false))
          continue;

        char promotion_figure = ' ';

        int moves_on_next;
        if (copy_with_move.promotion[0] != -1) {
          int pr[] = {copy_with_move.promotion[0], copy_with_move.promotion[1]};
          copy_with_move.promotion[0] = -1;
          copy_with_move.promotion[1] = -1;

          promotion_figure = pr[1] == 0 ? 'Q' : 'q';
          copy_with_move
              .board[pr[1]][pr[0]] =
              promotion_figure;
          
          moves_on_next = count_moves_on_depth(copy_with_move, depth - 1
#ifdef RAYLIB
                                               ,
                                               piece_set, board_theme
#endif
          );
          result += moves_on_next;

#ifdef RAYLIB
          graphic_print_board(copy_with_move, piece_set, board_theme);
#endif

          promotion_figure = pr[1] == 0 ? 'R' : 'r';
          copy_with_move
              .board[pr[1]][pr[0]] =
              promotion_figure;
          moves_on_next = count_moves_on_depth(copy_with_move, depth - 1
#ifdef RAYLIB
                                               ,
                                               piece_set, board_theme
#endif
          );
          result += moves_on_next;

#ifdef RAYLIB
          graphic_print_board(copy_with_move, piece_set, board_theme);
#endif

          promotion_figure = pr[1] == 0 ? 'B' : 'b';
          copy_with_move
              .board[pr[1]][pr[0]] =
              promotion_figure;
          moves_on_next = count_moves_on_depth(copy_with_move, depth - 1
#ifdef RAYLIB
                                               ,
                                               piece_set, board_theme
#endif
          );
          result += moves_on_next;
#ifdef RAYLIB
          graphic_print_board(copy_with_move, piece_set, board_theme);
#endif

          promotion_figure = pr[1] == 0 ? 'N' : 'n';
          copy_with_move
              .board[pr[1]][pr[0]] =
              promotion_figure;
          moves_on_next = count_moves_on_depth(copy_with_move, depth - 1
#ifdef RAYLIB
                                               ,
                                               piece_set, board_theme
#endif
          );
          result += moves_on_next;

#ifdef RAYLIB
          graphic_print_board(copy_with_move, piece_set, board_theme);
#endif

        } else {
          moves_on_next = count_moves_on_depth(copy_with_move, depth - 1
#ifdef RAYLIB
                                               ,
                                               piece_set, board_theme
#endif
          );
          result += moves_on_next;
#ifdef RAYLIB
          graphic_print_board(copy_with_move, piece_set, board_theme);
#endif
        }

#ifdef RAYLIB
        EndDrawing();
        usleep(5000);
#endif
        free_chess(copy_with_move);
      }
    }
  }

  return result;
}
