#include <stdlib.h>
#include <string.h>

#ifdef PLATFORM_ANDROID

char *my_strndup(char *str) {
  char *copy = malloc(100 * sizeof(char));

  if (copy == NULL)
    return NULL;

  strncpy(copy, str, 100);

  return copy;
}

#else
// FROM https://github.com/bminor/musl/blob/master/src/string/strndup.c

size_t my_strnlen(const char *s, size_t n) {
  const char *p = memchr(s, 0, n);
  return p ? p - s : n;
}

char *my_strndup(const char *s, size_t n) {
  size_t l = my_strnlen(s, n);
  char *d = malloc(l + 1);
  if (!d)
    return NULL;
  memcpy(d, s, l);
  d[l] = 0;
  return d;
}
#endif
