CC = clang
BIN = ./bin/main
MAIN = src/main.c
FORMAT = clang-format

# ADD NEW SOURCE FILES THESE
CFILES = src/engine.c src/utils.c src/cursor.c src/piece_set.c src/err.c src/chess.c src/figures.c src/fen.c
HFILES = src/c_chess.h
OBJFILES = obj_files/engine.o obj_files/utils.o obj_files/cursor.o obj_files/piece_set.o obj_files/err.o obj_files/chess.o obj_files/figures.o obj_files/fen.o

FMTFILES = $(CFILES:%.c=%.c.fmt) $(HFILES:%.h=%.h.fmt)

RAYLIBFLAGS = -DRAYLIB -I"/usr/local/include/" -L"/usr/local/lib/" -lraylib -lm -lpthread
CFLAGS = -std=c99 -Wall -Wextra -pedantic -ggdb -O3
MOUSE_OR_TOUCH = -DMOUSE

all: clean $(BIN)

$(BIN): $(OBJFILES)
	$(CC) $(CFLAGS) -o $@ $(MAIN) $^ # $(RAYLIBFLAGS) $(MOUSE_OR_TOUCH)

obj_files/%.o:src/%.c
	$(CC) $(CFLAGS) -c -o $@ $^ # $(RAYLIBFLAGS) $(MOUSE_OR_TOUCH)

fmt: $(FMTFILES)

%.c.fmt: %.c 
	$(FORMAT) --style=LLVM $< > $@
	mv $@ $<

%.h.fmt: %.h 
	$(FORMAT) --style=LLVM $< > $@
	mv $@ $<

clean:
	rm -rf $(OBJFILES) $(BIN)

run: all
	$(BIN)
